# docker image as base image
FROM nvidia/cuda:10.1-cudnn7-runtime

ENV PATH="/root/miniconda3/bin:${PATH}"
ARG PATH="/root/miniconda3/bin:${PATH}"
RUN apt-get update

RUN apt-get install -y wget && rm -rf /var/lib/apt/lists/*

RUN wget \
    https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && mkdir /root/.conda \
    && bash Miniconda3-latest-Linux-x86_64.sh -b \
    && rm -f Miniconda3-latest-Linux-x86_64.sh 

RUN conda update conda
RUN conda update --all
# Initialize conda in bash config fiiles:
RUN conda init bash

# Install libraries with conda
RUN conda install tsnecuda cuda101 -c cannylab
RUN conda install pytorch torchvision cudatoolkit=10.1 -c pytorch
RUN conda install -c conda-forge matplotlib
RUN conda install -c anaconda seaborn
