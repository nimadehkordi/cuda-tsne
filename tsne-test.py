import time
import matplotlib.pyplot as plt
import numpy as np
import torch
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
from tsnecuda import TSNE
from PIL import Image
import argparse
import re

class MyTSNE:
    def __init__(self, feat_path, pid_path, file_path , save_path):
        self.feat_path = feat_path
        self.save_path = save_path
        self.pid_path = pid_path
        self.file_path = file_path

    # compute tsne
    def compute(self):
        feats_torch = torch.load(self.feat_path).cpu().numpy()
        feats_tsne_embedded = TSNE(n_components=2, perplexity=50).fit_transform(feats_torch)
        np.save(self.save_path, feats_tsne_embedded)
        return feats_tsne_embedded

    # visualize
    def visualize(self, feats_tsne_embedded):
        pids = np.load(self.pid_path)
        paths = np.load(self.file_path)

        x = feats_tsne_embedded[:, 0]
        y = feats_tsne_embedded[:, 1]
        fig, ax = plt.subplots(figsize=(100, 100))

        _, indices = np.unique(pids, return_index=True)

        for x0, y0, path in zip(x[indices], y[indices], paths[indices]):
            #city = self._getCity(path)
            ab = AnnotationBbox(self._getImage(path.replace('/home/nima/projects/datasets', '/datasets')), (x0, y0),
                                frameon=False)
            ax.scatter(x0, y0)
            ax.add_artist(ab)

        plt.axis('off')
        plt.savefig('data/scatter.pdf')

    def _getCity(self, path):
        regex ='.*-city(.*)-scenario.*'
        m = re.search(regex, path)
        return  m.groups()[0]

    def _getImage(self, path):
        # load image
        img = Image.open(path).resize((64,128))
        pixels = np.asarray(img)
        # confirm pixel range is 0-255
        #print('Data Type: %s' % pixels.dtype)
        #print('Min: %.3f, Max: %.3f' % (pixels.min(), pixels.max()))
        # convert from integers to floats
        pixels = pixels.astype('float32')
        # normalize to the range 0-1
        pixels /= 255.0
        # confirm the normalization
        #print('Min: %.3f, Max: %.3f' % (pixels.min(), pixels.max()))
        return OffsetImage(pixels)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compute and visualize TSNE')
    parser.add_argument('--feat_path', type = str, default='data/ecp-softmax-ranked-features.pt',
                        help='path to extracted features from the model')
    parser.add_argument('--save_path', type=str, default='data/ecp-softmax-ranked-embedded.npy',
                        help='path to save the result of tsne')
    parser.add_argument('--pid_path', type=str, default='data/ecp-softmax-ranked-pids.npy',
                        help='path to pids')
    parser.add_argument('--file_path', type=str, default='data/ecp-softmax-ranked-paths.npy',
                        help='path to paths of images')
    args = parser.parse_args()

    t = time.time()
    ecp_tsne = MyTSNE(args.feat_path, args.pid_path, args.file_path, args.save_path)
    print('started computing tsne ...')
    feats_tsne_embedded = ecp_tsne.compute()
    feats_tsne_embedded = np.load(args.save_path)
    ecp_tsne.visualize(feats_tsne_embedded)
    print('time passed: {}'.format(time.time() - t))


