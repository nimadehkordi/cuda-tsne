# cuda-tsne-docker

Calculate TSNE based on [tsne-cuda](https://github.com/CannyLab/tsne-cuda)

Build the image with : 
`docker build --rm -t tsne:latest .`

Run the container with:
`docker run -it --rm --gpus all --mount type=bind,source="$(pwd)"/data,target=/root/data --mount type=bind,source=/home/nima/projects/Datasets,target=/root/Datasets -t tsne:latest`

`docker run -it --rm --gpus all --mount type=bind,source=/home/nima/projects,target=/home/nima/projects -t tsne:latest`

 